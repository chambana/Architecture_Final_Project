cmake_minimum_required(VERSION 3.6)
project(hough)

set(CMAKE_C_STANDARD 99)

set(SOURCE_FILES main.c)
add_executable(hough ${SOURCE_FILES})

target_link_libraries(hough m)  #link the math library