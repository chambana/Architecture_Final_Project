#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

#define THETA_DELTA 0.001
#define ROWS 1000000
#define COLS 4
#define return_data_tag 2002
#define data_count_msg_tag 2005

//mpicc main.c -std=gnu99 -lm

int main(int argc, char **argv) {

    int ierr, num_procs, my_id;
    MPI_Status status;

    printf("theta increment size = %i\n",(int)(3.14 / THETA_DELTA) );


    ierr = MPI_Init(&argc, &argv);
    ierr = MPI_Comm_rank(MPI_COMM_WORLD, &my_id);
    ierr = MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    //printf("Hello world! I'm process %i out of %i processes\n", my_id, num_procs);


    //printf("Hough Transform Code\n");

    FILE *fp;
    char buffer[255];
    char *lineptr;
    float theta_increments[(int)(3.14 / THETA_DELTA)];
    float **points = malloc(sizeof(float) * ROWS);

    int inc=0;
//    printf("calculating theta terms..\n");
//    for(float theta=0; theta<=3.14; theta+=THETA_DELTA)  //TODO:  your size is off by 1 in the length function
//    {
//        //printf("%f", theta);
//        theta_increments[inc]=theta;
//        printf("theta increments %d %f\n", inc, theta_increments[inc]);
//        inc+=1;
//    }
//    printf("size of theta increments %i\n", sizeof(theta_increments));


    //printf("opening file..\n");
    fp = fopen("points_and_ring.csv", "r");
    if(fp==NULL)
    {
        perror("error");
    }
    else
    {
        printf("file opened on process %i\n", my_id);
    }

    int row_count=0;
    //char* token;
    //buffer=char[255];
    char *tempptr;
    float val;

    printf("populating array from file..\n");

    while(fgets(buffer, 255, fp)) {
        //printf("grabbing data\n");
        points[row_count] = malloc(sizeof *points[row_count] * COLS);

        //printf("%s\n", buffer);
        //while ((token = strsep(&buffer, ","))) printf("%s",token);
        lineptr = buffer;
        tempptr = strsep(&lineptr, ",");

        int j=0;
        while (tempptr!=NULL)
        {
            val = atof(tempptr);
            //printf("%s %f", tempptr, val);

            //points[row_count][0]=double()
            points[row_count][j] =val;
            tempptr = strsep(&lineptr, ",");
            j+=1;
        }
        row_count+=1;
    }
    printf("proc %i is done grabbing data\n", my_id);


//
//    for (int row=0; row<row_count; row++)
//    {
//        for (int col=0; col<COLS; col++)
//        {
//            printf("%f-----", points[row][col]);
//        }
//        printf("\n");
//    }


    //float r_values_for_each_point[row_count][sizeof(theta_increments)/sizeof(theta_increments[0])+1];


    int slice_size = row_count/num_procs;
    printf("debug rowcount %i , %i\n", row_count  , (int)(sizeof(theta_increments)/sizeof(theta_increments[0])+1));

    int x= sizeof(theta_increments)/sizeof(theta_increments[0])+1;
    printf("%i\n",x);

    float **r_values_for_each_point = (float **)malloc(row_count*sizeof(float*));

    /*
     * From Huffmire's lecture this should be
     * int **a;
     * a=(int **)mallac(512*sizeof(int*);
     * for (int i=0; i<512; i++)
     * {
     *    a[i] = (int*)malloc(512 * sizeof(int));
     *
     */

    for (int i=0; i<row_count; i++){
        r_values_for_each_point[i] = (float*)malloc(( (int) (sizeof(theta_increments)/sizeof(theta_increments[0])+1)*sizeof(float)));
    }

    //float r_values_for_each_point[row_count][sizeof(theta_increments)/sizeof(theta_increments[0])+1];
    printf("calculating r values..\n");
    printf("process %i has slice size %i -- looping from %i to %i\n", my_id,slice_size, my_id* slice_size, (my_id* slice_size)+slice_size );

//    printf("array test for proc %i: %f", my_id, points[my_id* slice_size][0]);

    for (int row_num = my_id* slice_size; row_num<(my_id* slice_size)+slice_size; row_num++)
    {

        float x = points[row_num][0];
        float y = points[row_num][1];
        float z = points[row_num][2];
        float laser_num = points[row_num][3];

        for(int idx_theta=0; idx_theta<(sizeof(theta_increments)/sizeof(theta_increments[0])+1);idx_theta++)
        {
            r_values_for_each_point[row_num][idx_theta] = (float)(x * cos(theta_increments[idx_theta]) + y * sin(theta_increments[idx_theta]));
            //printf("Running theta=%f on point %f %f %f on laser %f r=%f\n" ,theta_increments[idx_theta], x,y,z,laser_num, r_values_for_each_point[row_num][idx_theta]);
//            if (my_id==1)
//            {
//                printf("Proc 2 calc'd index=%i   R=%f\n", row_num, r_values_for_each_point[row_num][idx_theta] );
//            }
        }

    }


    int root_process=0, i, num_rows, an_id, num_rows_to_receive, avg_rows_per_process, sender;

    //MASTER
    if (my_id==0)
    {
        printf("Running Master Node aggregation step.  Listening..\n");

        for( an_id = 1; an_id < num_procs; an_id++)
        {

            int num_rows_to_recv;
            printf("About to start blocking receive on ROOT\n");                     //TODO:  This is where Message Truncation is occuring!  A single message!!!  Try the iProbe



            //TODO:  I think that the sernder is stomping on the recv.  You are not providing enough space
            //int MPI_Iprobe(int source, int tag, MPI_Comm comm, int *flag, MPI_Status *status

            int flag = 0;
            int ret=0;
//            ret = MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
//            printf("iprobe returned flag  %i  \n", flag);


            ierr = MPI_Recv( &num_rows_to_receive, 1, MPI_INT, MPI_ANY_SOURCE,
                             data_count_msg_tag, MPI_COMM_WORLD, &status);

            printf("error %i\n", ierr);

            sender = status.MPI_SOURCE;
            printf("ROOT Received heads up from process %i that it's going to send %i rows\n", sender, num_rows_to_receive);

            int elem_count=0;
            //while (elem_count==0);
//            //{
//                ret = MPI_Get_count(&status, MPI_FLOAT, &elem_count);
//                printf("GET COUNT provide a count of: %i\n", elem_count);
//            //}

            ierr = MPI_Recv( &r_values_for_each_point[sender*slice_size][0], num_rows_to_receive, MPI_FLOAT,
                             MPI_ANY_SOURCE, return_data_tag, MPI_COMM_WORLD, &status);
            printf("error %i\n", ierr);


            printf("ROOT root process got a second recv payload from %i\n", sender);
            //printf("DEBUG\n");
//            for (int ic = 0; ic<(num_rows_to_receive/(sizeof(theta_increments)/sizeof(theta_increments[0])+1)); ic+=1)  //TODO:  This is wrong . numrows is that 12x number, it'll shoot pass the end of the array!!
//            {
//                printf("at %i got: %f", sender*slice_size+ic, r_values_for_each_point[sender*slice_size+ic][0]);
//            }
        }

    }



    //SLAVE
    else
    {
//        printf("DEBUG\n");
//        for (int row_num = my_id* slice_size; row_num<(my_id* slice_size)+slice_size; row_num++)
//        {
//            printf("sent: %f", r_values_for_each_point[row_num][0]);
//        }
        //printf("Running Slave Node %i aggregation step.  Sending...\n", my_id);

        int sizetosend =slice_size * (sizeof(theta_increments)/sizeof(theta_increments[0]));  //rows * thetas
        //printf("Slave Node sending %i.  Sending...\n", sizetosend);

        ierr = MPI_Send( &sizetosend, 1 , MPI_INT,
                         root_process, data_count_msg_tag, MPI_COMM_WORLD);
        printf("error %i\n", ierr);

        ierr = MPI_Send( &r_values_for_each_point[my_id * slice_size][0], sizetosend, MPI_FLOAT,
                         root_process, return_data_tag, MPI_COMM_WORLD);
        printf("error %i\n", ierr);

        printf("Slave #%i finished send heads up and payload\n", my_id);


    }

    //printf("number of rows: %d\n", row_count);
    //printf("size %d\n", sizeof(theta_increments)/sizeof(theta_increments[0]));



    ierr = MPI_Finalize();

    if (my_id==0)
    {
        printf("printing r values after FINALIZE inside myid=0\n");

        for (int index = 0; index < row_count; index += 10000) {
            //for(int idx_theta=0; idx_theta<(sizeof(theta_increments)/sizeof(theta_increments[0])+1);idx_theta++)
            //{
            printf("ind=%i,r=%f\n", index, r_values_for_each_point[index][101]); //print out a random element
            //}
        }
    }

    fclose(fp);







    //free memory
//    for (int row=0; row<ROWS; row++)
//    {
//        if (row%100==0)
//            printf("%i:%f ", row, points[row][0]);
//        //free(points[row]);
//
//    }


    //free memory
    for (int i=0; i<row_count; i++){
//        if (i%100==0)
//            printf("%f  ",r_values_for_each_point[i][10]);
        free(r_values_for_each_point[i]);
    }


    printf("closing down\n");


    return 0;
}


/*
 *
 * %% Part 2 - Hough Transform

thetas_vs_r_for_each_sonar_pt = [];
theta_increments = linspace(0,pi,pi/.1);
for idx = 1:size(map,1)
    for idx_theta = 1:length(theta_increments)
        x=map(idx,1);
        y=map(idx,2);
        r_values_for_each_sonar_pt(idx,idx_theta) = x*cos(theta_increments(idx_theta)) + y*sin(theta_increments(idx_theta));
    end

end


for idx = 1:size(r_values_for_each_sonar_pt,1)  %loop thru each row
    for idx_theta = 1:length(theta_increments)  %loop through each theta val
        r = r_values_for_each_sonar_pt(idx,idx_theta);
        theta = theta_increments(idx_theta);

    end
end


figure
plot(theta_increments, r_values_for_each_sonar_pt)
xlabel('theta');
ylabel('r value');
 *
 *
 *
 *
 * */








//    if (arr)
//    {
//        size_t i;
//        for (i = 0; i < ROWS; i++)
//        {
//            arr[i] = malloc(sizeof *arr[i] * COLS);
//            if (arr[i])
//            {
//                size_t j;
//                for (j = 0; j < COLS; j++)
//                {
//                    arr[i][j] = 7;
//                }
//            }
//        }
//    }
//
//    for (int i=0; i<ROWS; i++)
//    {
//        for (int j=0; j<3; j++)
//        {
//            printf("%d", arr[i][j]);
//        }
//        printf("\n");
//
//    }


//
//#include <stdlib.h>
//#include <string.h>
//#include <stdio.h>
//
//int main()
//{
//    char** array = malloc(1 * sizeof(*array));
//
//    if (array)
//    {
//        array[0] = "This";
//
//        printf("%s\n------\n", array[0]);
//
//        char** tmp = realloc(array, 2 * sizeof(*array));
//        if (tmp)
//        {
//            array = tmp;
//            array[1] = "That";
//
//            printf("%s\n", array[0]);
//            printf("%s\n", array[1]);
//        }
//
//        free(array);
//    }
//    return 0;
//}